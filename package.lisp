;;;; package.lisp
;;;;
;;;; Copyright (c) 2015 Robert Smith <quad@symbo1ics.com>

(defpackage #:murmurhash3
  (:use #:cl)
  (:export #:murmurhash32
           #:murmurhash32-generic
           #:murmurhash128
           #:murmurhash128-generic
           
           #:*default-seed*))

