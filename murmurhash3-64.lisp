;;;; murmurhash3-64.lisp
;;;;
;;;; Copyright (c) 2015 Robert Smith <quad@symbo1ics.com>

(in-package #:murmurhash3)

(defconstant $c64-4 #xFF51AFD7ED558CCD)
(defconstant $c64-5 #xC4CEB9FE1A85EC53)

(defun fmix64 (hash)
  (declare (type m64 hash))
  (setf hash (logxor hash (m64ash hash -33))
        hash (m64* hash $c64-4)
        hash (logxor hash (m64ash hash -33))
        hash (m64* hash $c64-5)
        hash (logxor hash (m64ash hash -33))))
