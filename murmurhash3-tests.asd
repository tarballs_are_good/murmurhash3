;;;; murmurhash3-tests.asd
;;;;
;;;; Copyright (c) 2015 Robert Smith <quad@symbo1ics.com>

(asdf:defsystem #:murmurhash3-tests
  :description "Tests for the MurmurHash3 library."
  :author "Robert Smith <quad@symbo1ics.com>"
  :license "BSD 3-clause (see LICENSE)"
  :depends-on (#:murmurhash3 #:trivial-garbage)
  :pathname #P"tests/"
  :serial t
  :components ((:file "package")
               (:file "murmurhash3-tests")
               (:file "murmurhash3-performance")))

(defmethod asdf:perform ((op asdf:test-op) (system (eql (asdf:find-system '#:murmurhash3))))
  (asdf:load-system '#:murmurhash3)
  (funcall (find-symbol "RUN-TESTS" :murmurhash3-tests)))
