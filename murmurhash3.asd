;;;; murmurhash3.asd
;;;;
;;;; Copyright (c) 2015 Robert Smith <quad@symbo1ics.com>

(asdf:defsystem #:murmurhash3
  :description "Implementation of MurmurHash3 (32-bit)."
  :author "Robert Smith <quad@symbo1ics.com>"
  :license "BSD 3-clause (see LICENSE.txt)"
  :depends-on (#+sbcl #:sb-rotate-byte)
  :serial t
  :components ((:file "package")
               (:file "utilities")
               (:file "murmurhash3-32")
               (:file "murmurhash3-64"))
  :in-order-to ((test-op (test-op murmurhash3-tests))))

