;;;; murmurhash3-32.lisp
;;;;
;;;; Copyright (c) 2015 Robert Smith <quad@symbo1ics.com>

(in-package #:murmurhash3)

#+#:safe
(declaim (optimize (speed 0) safety debug (space 0) (compilation-speed 0)))
;#+#:fast
(declaim (optimize speed
                   (safety 0)
                   (debug 0)
                   (space 0)
                   (compilation-speed 0)))


(defparameter *default-buffer-size* 1024
  "Number of 32-bit words for the file buffer size.")

(defparameter *default-seed* 42
  "Default seed used by MurmurHash.")

(defun check-length (length)
  (if (< length #.(ash 1 32))
      length
      (error "Length too large for Murmurhash3 (32-bit).")))


;;;;;;;;;;;;;;;;;;;;;;;;; MurmurHash3 32-bit ;;;;;;;;;;;;;;;;;;;;;;;;;

;;; MurmurHash3 (32-bit) magic constants

(defconstant $c32-1 #xCC9E2D51)
(defconstant $c32-2 #x1B873593)
(defconstant $c32-3 #xE6546B64)
(defconstant $c32-4 #x85EBCA6B)
(defconstant $c32-5 #xC2B2AE35)

(declaim (inline mix-block))
(defun mix-block (hash blk)
  (declare (type m32 hash blk))
  (setf blk (m32* blk $c32-1)
        blk (rotl32 blk 15)
        blk (m32* blk $c32-2))
  (logxor hash blk))

#+sbcl (declaim (sb-ext:maybe-inline mix-bulk))
#-sbcl (declaim (inline mix-bulk))
(defun mix-bulk (hash blk)
  (declare (type m32 hash blk))
  (setf hash (mix-block hash blk)
        hash (rotl32 hash 13)
        hash (m32+ $c32-3 (m32* 5 hash))))

#+sbcl (declaim (sb-ext:maybe-inline mix-tail))
#-sbcl (declaim (inline mix-tail))
(defun mix-tail (hash tail-blk)
  (declare (type m32 hash tail-blk))
  (mix-block hash tail-blk))

#+sbcl (declaim (sb-ext:maybe-inline fmix32))
#-sbcl (declaim (inline fmix32))
(defun fmix32 (hash)
  (declare (type m32 hash))
  (setf hash (logxor hash (m32ash hash -16))
        hash (m32* hash $c32-4)
        hash (logxor hash (m32ash hash -13))
        hash (m32* hash $c32-5)
        hash (logxor hash (m32ash hash -16))))

#+sbcl (declaim (sb-ext:maybe-inline final-processing-32))
#-sbcl (declaim (inline final-processing-32))
(defun final-processing-32 (hash bytes-hashed)
  (declare (type m32 hash bytes-hashed))
  (fmix32 (logxor hash bytes-hashed)))

(defun murmurhash3-32-file-stream (stream seed &key (buffer-size *default-buffer-size*))
  (declare (type m32 seed))
  (declare (inline mix-bulk mix-tail final-processing-32))
  
  (let* ((byte-length (check-length (file-length stream)))
         (buffer-length (* 4 buffer-size))
         (buffer (make-array buffer-length
                             :element-type 'octet
                             :initial-element 0))
         (hash seed))
    (declare (type m32 hash byte-length))
    (multiple-value-bind (block-count tail-count)
        (floor byte-length 4)
      (declare (type (integer 0 (4)) tail-count))
      ;; Mix in all of the main blocks. This is the core loop.
      (loop :with blocks-left :=  block-count
            :while (plusp blocks-left) :do
              (let ((bytes-read (read-sequence buffer stream)))
                ;; We read BLOCKS-READ. Decrease from the total number
                ;; we will need read hereafter.
                (decf blocks-left (floor bytes-read 4))

                ;; Construct the next word and mix it in.
                ;;
                ;; Instead of subtracting 3 from the upper bound, we
                ;; could do a (* 4 (floor bytes-read 4)) to in
                ;; effect round down to the nearest multiple of
                ;; 4. Subtracting 3 is sufficient since we are
                ;; counting by 4 already.
                (loop :for i :below (- bytes-read 3) :by 4
                      :for block :of-type m32
                        := (combine-bytes (aref buffer i)
                                          (aref buffer (+ 1 i))
                                          (aref buffer (+ 2 i))
                                          (aref buffer (+ 3 i)))
                      :do (setf hash (mix-bulk hash block)))))

      ;; If we have a tail block, find it and mix it in.
      (unless (zerop tail-count)
        (let ((last-buffer-length (mod byte-length buffer-length))
              (tail-block 0))
          (declare (type m32 tail-block))
          ;; The tail block might have been read in to the buffer,
          ;; or it might still be in the stream. Ensure it's in the
          ;; buffer.
          (when (or (zerop block-count)
                    (zerop last-buffer-length))
            (setf last-buffer-length (read-sequence buffer stream))
            (assert (= last-buffer-length tail-count)))
          
          (setf tail-block
                (ecase tail-count
                  ((1)
                   (aref buffer  (- last-buffer-length 1)))
                  ((2)
                   (combine-bytes
                    (aref buffer (- last-buffer-length 2))
                    (aref buffer (- last-buffer-length 1))))
                  ((3)
                   (combine-bytes
                    (aref buffer (- last-buffer-length 3))
                    (aref buffer (- last-buffer-length 2))
                    (aref buffer (- last-buffer-length 1))))))

          ;; Mix the tail block in.
          (setf hash (mix-tail hash tail-block))))
      
      ;; Perform the final processing.
      (values
       (final-processing-32 hash byte-length)
       byte-length))))

(defun murmurhash3-32-array (array seed)
  (declare (type m32 seed)
           (type (simple-array octet (*)) array))
  (declare (inline mix-bulk mix-tail final-processing-32))
  (let* ((byte-length (check-length (length array)))
         (hash seed))
    (declare (type m32 hash byte-length))
    (let ((tail-count (mod byte-length 4)))
      (declare (type (integer 0 (4)) tail-count))
      ;; Mix in all of the main blocks. This is the core loop.
      (loop :for i :below (- byte-length 3) :by 4
            :for block :of-type m32
              := (combine-bytes (aref array i)
                                (aref array (+ 1 i))
                                (aref array (+ 2 i))
                                (aref array (+ 3 i)))
            :do (setf hash (mix-bulk hash block)))

      ;; If we have a tail block, mix it in.
      (unless (zerop tail-count)
        (let ((tail-block 0))
          (declare (type m32 tail-block))
          (setf tail-block
                (ecase tail-count
                  ((1)
                   (aref array  (- byte-length 1)))
                  ((2)
                   (combine-bytes
                    (aref array (- byte-length 2))
                    (aref array (- byte-length 1))))
                  ((3)
                   (combine-bytes
                    (aref array (- byte-length 3))
                    (aref array (- byte-length 2))
                    (aref array (- byte-length 1))))))
          
          ;; Mix the tail block in.
          (setf hash (mix-tail hash tail-block))))
      
      ;; Perform the final processing.
      (values
       (final-processing-32 hash byte-length)
       byte-length))))


;;;;;;;;;;;;;;;;; MurmurHash3 32-bit, 128-bit output ;;;;;;;;;;;;;;;;;

;;; MurmurHash3 (128-bit) Magic constants.

(defconstant $c32-128-1 #x239B961B)
(defconstant $c32-128-2 #xAB0E9789)
(defconstant $c32-128-3 #x38B34AE5)
(defconstant $c32-128-4 #xA1E38B93)
(defconstant $c32-128-5 #x561CCD1B)
(defconstant $c32-128-6 #x0BCAA747)
(defconstant $c32-128-7 #x96CD1C35)
(defconstant $c32-128-8 #x32AC3B17)

(declaim (inline final-processing-32-128))
(defun final-processing-32-128 (hash1 hash2 hash3 hash4 byte-length)
  (declare (type m32 hash1 hash2 hash3 hash4 byte-length))
  (setf hash1 (logxor hash1 byte-length)
        hash2 (logxor hash2 byte-length)
        hash3 (logxor hash3 byte-length)
        hash4 (logxor hash4 byte-length))
  (setf hash1 (m32+ hash1 hash2)
        hash1 (m32+ hash1 hash3)
        hash1 (m32+ hash1 hash4)
        
        hash2 (m32+ hash2 hash1)
        hash3 (m32+ hash3 hash1)
        hash4 (m32+ hash4 hash1))
  (setf hash1 (fmix32 hash1)
        hash2 (fmix32 hash2)
        hash3 (fmix32 hash3)
        hash4 (fmix32 hash4))
  (setf hash1 (m32+ hash1 hash2)
        hash1 (m32+ hash1 hash3)
        hash1 (m32+ hash1 hash4)
        
        hash2 (m32+ hash2 hash1)
        hash3 (m32+ hash3 hash1)
        hash4 (m32+ hash4 hash1))
  (values hash1 hash2 hash3 hash4))

(declaim (inline mix-bulk-32-128))
(defun mix-bulk-32-128 (hash1 hash2 hash3 hash4 block1 block2 block3 block4)
  (declare (type m32 hash1 hash2 hash3 hash4 block1 block2 block3 block4))
  (setf block1 (m32* block1 $c32-128-1)
        block1 (rotl32 block1 15)
        block1 (m32* block1 $c32-128-2)
        hash1 (logxor hash1 block1)
        hash1 (rotl32 hash1 19)
        hash1 (m32+ hash1 hash2)
        hash1 (m32+ $c32-128-5 (m32* 5 hash1)))
  (setf block2 (m32* block2 $c32-128-2)
        block2 (rotl32 block2 16)
        block2 (m32* block2 $c32-128-3)
        hash2 (logxor hash2 block2)
        hash2 (rotl32 hash2 17)
        hash2 (m32+ hash2 hash3)
        hash2 (m32+ $c32-128-6 (m32* 5 hash2)))
  (setf block3 (m32* block3 $c32-128-3)
        block3 (rotl32 block3 17)
        block3 (m32* block3 $c32-128-4)
        hash3 (logxor hash3 block3)
        hash3 (rotl32 hash3 15)
        hash3 (m32+ hash3 hash4)
        hash3 (m32+ $c32-128-7 (m32* 5 hash3)))
  (setf block4 (m32* block4 $c32-128-4)
        block4 (rotl32 block4 18)
        block4 (m32* block4 $c32-128-1)
        hash4 (logxor hash4 block4)
        hash4 (rotl32 hash4 13)
        hash4 (m32+ hash4 hash1)
        hash4 (m32+ $c32-128-8 (m32* 5 hash4)))
  (values hash1 hash2 hash3 hash4))

(declaim (inline extract-blocks-and-mix-tail-32-128))
(defun extract-blocks-and-mix-tail-32-128 (bytes-remaining array start hash1 hash2 hash3 hash4)
  "Extract the tail blocks from the array ARRAY from the starting point START"
  (declare (type m32 bytes-remaining start hash1 hash2 hash3 hash4)
           (type (simple-array octet (*)) array))
  (let ((block1 0)
        (block2 0)
        (block3 0)
        (block4 0))
    (declare (type m32 block1 block2 block3 block4))
    (switch bytes-remaining
      (15 (setf block4 (logxor block4 (m32ash (aref array (+ start 14)) 16))))
      (14 (setf block4 (logxor block4 (m32ash (aref array (+ start 13)) 8))))
      (13 (setf block4 (logxor block4         (aref array (+ start 12)))
                block4 (m32* block4 $c32-128-4)
                block4 (rotl32 block4 18)
                block4 (m32* block4 $c32-128-1)
                hash4 (logxor hash4 block4)))
      
      (12 (setf block3 (logxor block3 (m32ash (aref array (+ start 11)) 24))))
      (11 (setf block3 (logxor block3 (m32ash (aref array (+ start 10)) 16))))
      (10 (setf block3 (logxor block3 (m32ash (aref array (+ start 9)) 8))))
      (9  (setf block3 (logxor block3         (aref array (+ start 8)))
                block3 (m32* block3 $c32-128-3)
                block3 (rotl32 block3 17)
                block3 (m32* block3 $c32-128-4)
                hash3 (logxor hash3 block3)))
      
      (8  (setf block2 (logxor block2 (m32ash (aref array (+ start 7)) 24))))
      (7  (setf block2 (logxor block2 (m32ash (aref array (+ start 6)) 16))))
      (6  (setf block2 (logxor block2 (m32ash (aref array (+ start 5)) 8))))
      (5  (setf block2 (logxor block2         (aref array (+ start 4)))
                block2 (m32* block2 $c32-128-2)
                block2 (rotl32 block2 16)
                block2 (m32* block2 $c32-128-3)
                hash2 (logxor hash2 block2)))
      
      (4  (setf block1 (logxor block1 (m32ash (aref array (+ start 3)) 24))))
      (3  (setf block1 (logxor block1 (m32ash (aref array (+ start 2)) 16))))
      (2  (setf block1 (logxor block1 (m32ash (aref array (+ start 1)) 8))))
      (1  (setf block1 (logxor block1        (aref array start))
                block1 (m32* block1 $c32-128-1)
                block1 (rotl32 block1 15)
                block1 (m32* block1 $c32-128-2)
                hash1 (logxor hash1 block1))))
    (values hash1 hash2 hash3 hash4)))

(defun murmurhash3-32-128-array (array seed)
  (declare (type m32 seed)
           (type (simple-array octet (*)) array))
  (let* ((byte-length (check-length (length array)))
         (hash1 seed)
         (hash2 seed)
         (hash3 seed)
         (hash4 seed))
    (declare (type m32 hash1 hash2 hash3 hash4 byte-length))
    (let ((tail-count (mod byte-length 16)))
      ;; Mix in all of the main blocks. This is the core loop.
      (loop :for i :below (- byte-length 15) :by 16
            ;; Get the blocks.
            :for block1 := (combine-bytes (aref array i)
                                          (aref array (+ 1 i))
                                          (aref array (+ 2 i))
                                          (aref array (+ 3 i)))
            :for block2 := (combine-bytes (aref array (+ 4 i))
                                          (aref array (+ 5 i))
                                          (aref array (+ 6 i))
                                          (aref array (+ 7 i)))
            :for block3 := (combine-bytes (aref array (+ 8 i))
                                          (aref array (+ 9 i))
                                          (aref array (+ 10 i))
                                          (aref array (+ 11 i)))
            :for block4 := (combine-bytes (aref array (+ 12 i))
                                          (aref array (+ 13 i))
                                          (aref array (+ 14 i))
                                          (aref array (+ 15 i)))
            ;; Mix them into the hash.
            :do (multiple-value-setq (hash1 hash2 hash3 hash4)
                  (mix-bulk-32-128 hash1 hash2 hash3 hash4
                                   block1 block2 block3 block4)))
      
      ;; If we have a tail block, mix it in.
      (unless (zerop tail-count)
        (multiple-value-setq (hash1 hash2 hash3 hash4)
          (extract-blocks-and-mix-tail-32-128 tail-count
                                              array
                                              (- byte-length tail-count)
                                              hash1 hash2 hash3 hash4)))
      
      ;; Perform the final processing.
      (multiple-value-setq (hash1 hash2 hash3 hash4)
        (final-processing-32-128 hash1 hash2 hash3 hash4 byte-length))
      
      (values hash1 hash2 hash3 hash4 byte-length))))

(defun murmurhash3-32-128-file-stream (stream seed &key (buffer-size *default-buffer-size*))
  (declare (type m32 seed))
  (let* ((byte-length (check-length (file-length stream)))
         (buffer-length (* 16 (floor buffer-size 4)))
         (buffer (make-array buffer-length :element-type 'octet
                                           :initial-element 0))
         (hash1 seed)
         (hash2 seed)
         (hash3 seed)
         (hash4 seed))
    (declare (type m32 byte-length))
    (multiple-value-bind (superblock-count tail-count)
        (floor byte-length 16)
      (loop
        :with superblocks-left := superblock-count
        :while (plusp superblocks-left) :do
          (let ((bytes-read (read-sequence buffer stream)))
            (decf superblocks-left (floor bytes-read 16))
            ;; Mix in all of the main blocks. This is the core loop.
            (loop :for i :below (- bytes-read 15) :by 16
                  :for block1 := (combine-bytes (aref buffer i)
                                                (aref buffer (+ 1 i))
                                                (aref buffer (+ 2 i))
                                                (aref buffer (+ 3 i)))
                  :for block2 := (combine-bytes (aref buffer (+ 4 i))
                                                (aref buffer (+ 5 i))
                                                (aref buffer (+ 6 i))
                                                (aref buffer (+ 7 i)))
                  :for block3 := (combine-bytes (aref buffer (+ 8 i))
                                                (aref buffer (+ 9 i))
                                                (aref buffer (+ 10 i))
                                                (aref buffer (+ 11 i)))
                  :for block4 := (combine-bytes (aref buffer (+ 12 i))
                                                (aref buffer (+ 13 i))
                                                (aref buffer (+ 14 i))
                                                (aref buffer (+ 15 i)))
                  ;; Mix them into the hash.
                  :do (multiple-value-setq (hash1 hash2 hash3 hash4)
                        (mix-bulk-32-128 hash1 hash2 hash3 hash4
                                         block1 block2 block3 block4)))))
      
      ;; If we have tail blocks, extract them and mix them in.
      (unless (zerop tail-count)
        (let ((last-buffer-length (mod byte-length buffer-length)))
          (when (or (zerop superblock-count)
                    (zerop last-buffer-length))
            (setf last-buffer-length (read-sequence buffer stream))
            (assert (= last-buffer-length tail-count)))

          (multiple-value-setq (hash1 hash2 hash3 hash4)
            (extract-blocks-and-mix-tail-32-128 tail-count
                                                buffer
                                                (- last-buffer-length tail-count)
                                                hash1 hash2 hash3 hash4))))
      
      ;; Perform the final processing.
      (multiple-value-setq (hash1 hash2 hash3 hash4)
        (final-processing-32-128 hash1 hash2 hash3 hash4 byte-length))
      
      (values hash1 hash2 hash3 hash4 byte-length))))


;;; API functions

(defgeneric murmurhash32-generic (object seed)
  (:documentation "Generic 32-bit MurmurHash3 function, specializing on OBJECT. Note that further type discrimination may occur within the method implementations."))

(defun murmurhash32 (object &key (seed *default-seed*))
  "Compute the 32-bit MurmurHash3 of the object OBJECT with the seed SEED.

Returns two values:

    1: 32-bit value of the hash.
    2: Number of bytes hashed.

OBJECT may be one of a variety of objects as accepted by MURMURHASH32-GENERIC."
  (check-type seed m32)
  (murmurhash32-generic object seed))

(defmethod murmurhash32-generic ((object vector) seed)
  (etypecase object
    ((simple-array octet (*))
     (murmurhash3-32-array object seed))))

(defmethod murmurhash32-generic ((object pathname) seed)
  (with-open-file (stream object :direction ':input
                                 :element-type 'octet)
    (murmurhash32-generic stream seed)))

(defmethod murmurhash32-generic ((object file-stream) seed)
  (assert (subtypep (stream-element-type object) 'octet))
  (assert (input-stream-p object))
  
  (murmurhash3-32-file-stream object seed))

;;; FIXME: Currently inefficient.
(defmethod murmurhash32-generic ((object string) seed)
  (assert (extended-ascii-string-p object))
  (let ((vector (make-array (length object) :element-type 'octet
                                            :initial-element 0)))
    (setf vector (map-into vector #'char-code object))
    (murmurhash32-generic vector seed)))


(defgeneric murmurhash128-generic (object seed)
  (:documentation "Generic 128-bit MurmurHash3 function, specializing on OBJECT. Note that further type discrimination may occur within the method implementations."))

(defun murmurhash128 (object &key (seed *default-seed*))
  "Compute the 128-bit MurmurHash3 of the object OBJECT.

Returns five values:

    1-4: 32-bit values of the hash.
      5: Number of bytes hashed.

OBJECT may be one of a variety of objects as accepted by MURMURHASH128-GENERIC."
  (check-type seed m32)
  (murmurhash128-generic object seed))

(defmethod murmurhash128-generic ((object vector) seed)
  (etypecase object
    ((simple-array octet (*))
     (murmurhash3-32-128-array object seed))))

(defmethod murmurhash128-generic ((object pathname) seed)
  (with-open-file (stream object :direction ':input
                                 :element-type 'octet)
    (murmurhash128-generic stream seed)))

(defmethod murmurhash128-generic ((object file-stream) seed)
  (assert (subtypep (stream-element-type object) 'octet))
  (assert (input-stream-p object))

  (murmurhash3-32-128-file-stream object seed))

;;; FIXME: Currently inefficient.
(defmethod murmurhash128-generic ((object string) seed)
  (assert (extended-ascii-string-p object))
  (let ((vector (make-array (length object) :element-type 'octet
                                            :initial-element 0)))
    (setf vector (map-into vector #'char-code object))
    (murmurhash128-generic vector seed)))
