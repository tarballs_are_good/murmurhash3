;;;; utilities.lisp
;;;;
;;;; Copyright (c) 2015 Robert Smith <quad@symbo1ics.com>

(in-package #:murmurhash3)

(deftype octet (&optional (length 1))
  `(unsigned-byte ,(* 8 length)))

(deftype m32 ()
  `(octet 4))

(deftype m32-count ()
  `(integer -31 31))

(deftype m64 ()
  `(octet 8))

(deftype m64-count ()
  `(integer -63 63))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun emit-byte-combiner (byte-variables)
    (loop :for offset :from 0 :by 8
          :for variable :in byte-variables
          :if (zerop offset)
            :collect variable :into form
          :else
            :collect `(ash ,variable ,offset) :into form
          :finally (return `(logior ,@form)))))

(defmacro combine-bytes (&rest bytes)
  (let ((gensyms (mapcar (lambda (x) (declare (ignore x)) (gensym)) bytes)))
    `(let ,(mapcar #'list gensyms bytes)
       (declare (type octet ,@gensyms))
       ,(emit-byte-combiner gensyms))))

(macrolet ((define-32-bit-function (name (operator arg-type-1 arg-type-2))
             `(progn
                (declaim (inline ,name))
                (defun ,name (a b)
                  (declare (type ,arg-type-1 a)
                           (type ,arg-type-2 b))
                  #-lispworks
                  (the m32 (ldb '#.(byte 32 0) (,operator a b)))
                  #+lispworks
                  (the m32 (logand #.(1- (ash 1 32)) (,operator a b)))))))
  (define-32-bit-function m32+ (+ m32 m32))
  (define-32-bit-function m32- (- m32 m32))
  (define-32-bit-function m32* (* m32 m32))
  (define-32-bit-function m32ash (ash m32 m32-count)))

(macrolet ((define-64-bit-function (name (operator arg-type-1 arg-type-2))
             `(progn
                (declaim (inline ,name))
                (defun ,name (a b)
                  (declare (type ,arg-type-1 a)
                           (type ,arg-type-2 b))
                  #-lispworks
                  (the m64 (ldb '#.(byte 64 0) (,operator a b)))
                  #+lispworks
                  (the m64 (logand #.(1- (ash 1 64)) (,operator a b)))))))
  (define-64-bit-function m64+ (+ m64 m64))
  (define-64-bit-function m64- (- m64 m64))
  (define-64-bit-function m64* (* m64 m64))
  (define-64-bit-function m64ash (ash m64 m64-count)))

(declaim (inline rotl32))
(defun rotl32 (x r)
  (declare (type m32 x)
           (type m32-count r))
  #-sbcl
  (logior
   (m32ash x r)
   (m32ash x (- r 32)))
  #+sbcl
  (sb-rotate-byte:rotate-byte r '#.(byte 32 0) x))

(declaim (inline rotl64))
(defun rotl64 (x r)
  (declare (type m64 x)
           (type m64-count r))
  #-sbcl
  (logior
   (m64ash x r)
   (m64ash x (- r 64)))
  #+sbcl
  (sb-rotate-byte:rotate-byte r '#.(byte 64 0) x))

(defmacro switch (expr &body case-clauses)
  (let ((case-values (mapcar #'car case-clauses))
        (case-labels (mapcar (lambda (x) (declare (ignore x)) (gensym "LABEL"))
                             case-clauses)))
    `(tagbody
        (ecase ,expr
          ,@(loop :for value :in case-values
                  :for label :in case-labels
                  :collect `((,value) (go ,label))))
        ,@(loop :for label :in case-labels
                :for case-progn :in (mapcar #'cdr case-clauses)
                :collect label
                :append case-progn))))

(defun extended-ascii-character-p (character)
  (<= 0 (char-code character) 255))

(defun extended-ascii-string-p (string)
  (every #'extended-ascii-character-p string))
