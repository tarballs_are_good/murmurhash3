;;;; tests/package.lisp
;;;;
;;;; Copyright (c) 2015 Robert Smith <quad@symbo1ics.com>

(defpackage #:murmurhash3-tests
  (:use #:cl #:murmurhash3)
  (:export #:test-murmurhash32-array-correctness
           #:test-murmurhash32-file-correctness
           #:test-murmurhash128-file-correctness
           #:run-tests
           #:test-performance-32
           #:test-performance-128))

