;;;; murmurhash3-tests.asd
;;;;
;;;; Copyright (c) 2015 Robert Smith <quad@symbo1ics.com>

(in-package #:murmurhash3-tests)

(defun random-bytevector (length)
  (check-type length (integer 1))
  (let ((v (make-array length :element-type '(unsigned-byte 8)
                              :initial-element 0)))
    (loop :for i :below length
          :do (setf (aref v i) (random 256))
          :finally (return v))))

(defun timeit (thunk)
  (let ((start (get-internal-real-time)))
    (funcall thunk)
    (float (/ (- (get-internal-real-time) start)
              internal-time-units-per-second)
           1.0d0)))

(defun pretty-print-unit (size)
  (let* ((K 1024)
         (M (* 1024 K))
         (G (* 1024 M)))
    (cond
      ((<= 0 size 999)             (format nil "~3D B" (round size)))
      ((<= 1000 size 999999)       (format nil "~3,2F KB" (/ size K)))
      ((<= 1000000 size 999999999) (format nil "~3,2F MB" (/ size M)))
      (t                           (format nil "~,2F GB" (/ size G))))))

(defun test-performance-32 (&key (max-kb #.(* 512 1024)))
  (loop :for i := 1 :then (* 2 i)
        :while (< i max-kb)
        :do (let* ((size (* i 1024))
                   (v (random-bytevector size)))
              (trivial-garbage:gc :full t)
              (let ((time (timeit (lambda () (murmurhash3:murmurhash32 v)))))
                (format t "~A in ~F s @ ~A/s~%"
                        (pretty-print-unit size)
                        time
                        (if (zerop time)
                            "inf MB"
                            (pretty-print-unit (/ size time))))))))

(defun test-performance-128 (&key (max-kb #.(* 512 1024)))
  (loop :for i := 1 :then (* 2 i)
        :while (< i max-kb)
        :do (let* ((size (* i 1024))
                   (v (random-bytevector size)))
              (trivial-garbage:gc :full t)
              (let ((time (timeit (lambda () (murmurhash3:murmurhash128 v)))))
                (format t "~A in ~F s @ ~A/s~%"
                        (pretty-print-unit size)
                        time
                        (if (zerop time)
                            "inf MB"
                            (pretty-print-unit (/ size time))))))))
