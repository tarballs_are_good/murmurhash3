;;;; tests/murmurhash3-tests.lisp
;;;;
;;;; Copyright (c) 2015 Robert Smith <quad@symbo1ics.com>

(in-package #:murmurhash3-tests)

(defparameter *test-hashes*
  '(("" #x087fcd5c #xaf6d2cb6 #x95c80cba #x95c80cba #x95c80cba)
    ("a" #xb2e5a263 #x517b4f52 #x94460fc6 #x94460fc6 #x94460fc6)
    ("ab" #xd72d0e47 #xd33999e7 #x82bc5a64 #x82bc5a64 #x82bc5a64)
    ("abc" #x4e4f1e68 #x5bcf0fbe #x8741bbe9 #x8741bbe9 #x8741bbe9)
    ("abcd" #xe860e5cc #xc4c3e662 #xb5ddd2d4 #xb5ddd2d4 #xb5ddd2d4)
    ("abcde" #xaeda2bf0 #x583d8e9f #x09f0e63b #xdeafe2a7 #xdeafe2a7)
    ("abcdef" #x91fd060b #x3ada56af #x86a9117f #xc9691239 #xc9691239)
    ("abcdefg" #x6a2af619 #x218d97b7 #x1193f6fd #xc329021b #xc329021b)
    ("abcdefgh" #xd0089d49 #x8fa25d73 #xb472606c #xf78bb83d #xf78bb83d)
    ("abcdefghi" #x4023b084 #xc60274fd #x18290c7b #x31b94639 #x4f400945)
    ("abcdefghij" #x9a0da499 #x23b100e2 #xf1bd6c11 #x2f11c760 #x06b33df5)
    ("abcdefghijk" #x86a4020d #x448e8960 #xfeceebcd #x6d663eb4 #x5db70807)
    ("abcdefghijkl" #xb4384448 #xff5d9f50 #x359b8f2b #x78e3adc2 #xd06ada6f)
    ("abcdefghijklm" #xfa62953b #x25036234 #xc48a898e #xb4c5c204 #xabd0e578)
    ("abcdefghijklmn" #x30d15a7a #xb1f90bc4 #x9816df76 #x20a41ad8 #xfae8f74d)
    ("abcdefghijklmno" #x8fa79184 #xf39f4b16 #xe6e96a2c #x449b4082 #x4267d2a5)
    ("abcdefghijklmnop" #x95374992 #x3a3ab14e #x9feddf15 #xd15afd29 #xe6975eb0)
    ("abcdefghijklmnopq" #xb5825556 #xb3ea3c14 #xeeb9e952 #x01ac8ed2 #x7abb486e)
    ("abcdefghijklmnopqr" #x3629af80 #xd784a041 #x44c98c0a #x9ce23218 #x8bce511c)
    ("abcdefghijklmnopqrs" #x890fe5dd #x38c71d57 #x1e6edf79 #x19c22f02 #x70aac2ef)
    ("abcdefghijklmnopqrst" #xef02f3c5 #x1ae32e1c #xc184e087 #x8b8d70bb #xd550db85)
    ("abcdefghijklmnopqrstu" #x2a81f215 #x5dd8d888 #x5f6736de #x80911ee3 #x6f0a6d07)
    ("abcdefghijklmnopqrstuv" #xafe2c1f1 #x6a510b30 #xb82a0d38 #xd08c75fe #x1c662612)
    ("abcdefghijklmnopqrstuvw" #xb390fd55 #x6eae6f22 #xfe6bfbc1 #xdf96a523 #x5c26636f)
    ("abcdefghijklmnopqrstuvwx" #xf3b5a6bf #x174aaff7 #x53c41cea #x024c14fe #x2b015db5)
    ("abcdefghijklmnopqrstuvwxy" #x568b04f7 #x14caa631 #x7885021f #x82ee642b #x33c3072d)
    ("abcdefghijklmnopqrstuvwxyz" #x7066cdfe #xacf85d33 #xb054266b #x11f3ee29 #x9bccb27c)
    ("abcdefghijklmnopqrstuvwxyz0" #x80ad3952 #x1fa50df3 #xd34671b4 #x0717f174 #xaeb8d2a3)
    ("abcdefghijklmnopqrstuvwxyz01" #xb304dbb8 #x1c0737fb #x68929828 #xa933098c #x3076950c)
    ("abcdefghijklmnopqrstuvwxyz012" #x07b69fc8 #x45320d6c #x6d2bd305 #xa0d26ab0 #x004838b3)
    ("abcdefghijklmnopqrstuvwxyz0123" #x26dd136c #x3f57a54e #x7285d680 #xd467f329 #x12387a98)
    ("abcdefghijklmnopqrstuvwxyz01234" #xe888a0e2 #xd175b825 #xc85d73ed #x05df7fae #xcad51111)
    ("abcdefghijklmnopqrstuvwxyz012345" #x96f5a04c #xea84f256 #x21e4d5c7 #x5c81b841 #x7d0989e4)
    ("abcdefghijklmnopqrstuvwxyz0123456" #x2283634f #x79ee3171 #x99b055df #xd53028f5 #x7bbe2dd9)
    ("abcdefghijklmnopqrstuvwxyz01234567" #xa92cfc8e #xbbfe8086 #x9d80289f #xf4da97a2 #x2cda5859)
    ("abcdefghijklmnopqrstuvwxyz012345678" #xddfaa081 #xca64ab14 #xe60180ad #x2454df16 #xcadea486)
    ("abcdefghijklmnopqrstuvwxyz0123456789" #x02c5aa9f #xc6eb67d0 #xf3edf304 #x79a83ead #xb1c82e3b)))

(defun test-murmurhash3-string (string)
  (let* ((v (make-array (length string)
                        :element-type '(unsigned-byte 8)
                        :initial-contents (map 'list #'char-code string)))
         (h (murmurhash3::murmurhash32 v :seed 42)))
    (multiple-value-bind (h1 h2 h3 h4) (murmurhash3:murmurhash128 v :seed 42)
      (list h h1 h2 h3 h4))))

(defun test-murmurhash-array-correctness ()
  (loop :with errors := 0
        :for (string . hashes) :in *test-hashes*
        :for calculated-hashes := (test-murmurhash3-string string)
        :unless (equalp hashes calculated-hashes)
          :do (warn "Mismatch in hashes for string ~S:~%~
                     ~3TExpected: ~S~%~
                     ~3TReceived: ~S~%"
                    string hashes calculated-hashes)
              (incf errors)
        :finally (when (plusp errors)
                   (format t "~&!!! There were ~D errors.~%" errors))
                 (return (zerop errors))))

(defun test-murmurhash32-file-correctness (filename)
  (with-open-file (stream filename :direction :input
                                   :element-type '(unsigned-byte 8))
    (let ((buffer (make-array (file-length stream) :element-type '(unsigned-byte 8)
                                                   :initial-element 0)))
      (read-sequence buffer stream)
      (let ((a (murmurhash3::murmurhash32 buffer))
            (b (murmurhash3::murmurhash32 filename))) 
        (format t "~&Buffer: ~X~%" a)
        (format t "File  : ~X~%" b)
        (= a b)))))

(defun test-murmurhash128-file-correctness (filename)
  (with-open-file (stream filename :direction :input
                                   :element-type '(unsigned-byte 8))
    (let ((buffer (make-array (file-length stream) :element-type '(unsigned-byte 8)
                                                   :initial-element 0)))
      (read-sequence buffer stream)
      (multiple-value-bind (a1 a2 a3 a4) (murmurhash3:murmurhash128 buffer)
        (multiple-value-bind (b1 b2 b3 b4) (murmurhash3:murmurhash128 filename)
          (format t "~&Buffer: ~X ~X ~X ~X~%" a1 a2 a3 a4)
          (format t "File  : ~X ~X ~X ~X~%" b1 b2 b3 b4)
          (and (= a1 b1)
               (= a2 b2)
               (= a3 b3)
               (= a4 b4)))))))

(defun run-tests ()
  "Run all correctness tests."
  (if (test-murmurhash-array-correctness)
      (format t "~&All tests passed.~%")
      (format t "~&There were test failures.~%")))
